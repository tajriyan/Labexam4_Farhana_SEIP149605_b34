<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <style>
        .carousel-inner > .item > img,
        .carousel-inner > .item > a > img {
            width: 70%;
            margin: auto;
        }
    </style>
</head>
<body>
<form action="task2.php" method="post" enctype="multipart/form-data">
    <div class="container">
        <h2>Modal </h2>
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">click for  Modal</button>

        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <Input type="text" name="name">
                        <h4 class="modal-title">Name</h4>
                        </div>
                    <div class="modal-body">
                        <Input type="date" name="date">
                        <h4 class="modal-title">Date</h4>
                    </div>
                    <div class="modal-body">
                        <h3 class="modal-title">Select a Image</h3>
                        <input type="file" name="filetoupload">.
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="send" value="send">
                    </div>
                </div>

            </div>
        </div>

    </div>
    </form>

</body>
</html>